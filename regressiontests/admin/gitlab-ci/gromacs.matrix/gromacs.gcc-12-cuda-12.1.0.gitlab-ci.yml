# Test goal: old versions of GCC with CUDA; GPU communications with tMPI (default)
# Test intents (should change rarely and conservatively):
#   OS: Ubuntu oldest supported on NGC with the chosen CUDA
#   Compiler: GCC oldest supported with the chosen CUDA
#   GPU: CUDA oldest supported
#   HW: NVIDIA GPU, dual NVIDIA GPU
#   MPI: thread_MPI
#   Features: GPU direct communications + update (unit tests)
#   Features: GPU direct communications + update (regression tests with dual GPU)
#   Features: GPU update (regression tests with dual GPU)
#   Scope: configure, build, unit tests, regression tests
# Test implementation choices (free to change as needed):
#   OS: Ubuntu 22.04
#   Build type: RelWithAssert
#   Compiler: GCC 12.2
#   GPU: CUDA 12.1.0
#   GPU direct communication: on / default
#   SIMD: SSE 4.1
#   FFT: FFTW3
#   Parallelism nt/ntomp: 4/2 (unit tests)
#   Parallelism nt/ntomp: 2/1 (regression tests)
#   Parallelism nt/ntomp: 4/1 (regression tests with dual GPU)

gromacs:gcc-12-cuda-12.1.0:configure:
  extends:
    - .gromacs:base:configure
    - .use-gcc:base
    - .use-cuda
  rules:
    - !reference [.rules:merge-requests, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-12-cuda-12.1.0
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
    CMAKE_BUILD_TYPE_OPTIONS : "-DCMAKE_BUILD_TYPE=RelWithAssert"
    CMAKE_SIMD_OPTIONS: "-DGMX_SIMD=SSE4.1"
    COMPILER_MAJOR_VERSION: 12

gromacs:gcc-12-cuda-12.1.0:build:
  extends:
    - .gromacs:base:build
    - .before_script:default
    - .use-ccache
  rules:
    - !reference [.rules:merge-requests, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-12-cuda-12.1.0
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
  needs:
    - job: gromacs:gcc-12-cuda-12.1.0:configure
    - job: gromacs:prepare

gromacs:gcc-12-cuda-12.1.0:regressiontest:
  extends:
    - .gromacs:base:regressiontest
  rules:
    - !reference [.rules:skip-if-single-nvidia-gpu-unavailable, rules]
    - !reference [.rules:merge-requests-allow-failure, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-12-cuda-12.1.0
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
    REGRESSIONTEST_PME_RANK_NUMBER: 0
    REGRESSIONTEST_TOTAL_RANK_NUMBER: 2
    REGRESSIONTEST_OMP_RANK_NUMBER: 1
    GPU_VENDOR: "NVIDIA"
    GPU_COUNT: 1
  tags:
    - $GITLAB_RUNNER_TAG_1X_NVIDIA_GPU
  needs:
    - job: gromacs:gcc-12-cuda-12.1.0:build
    - job: gromacs:prepare

gromacs:gcc-12-cuda-12.1.0:regressiontest-gpucommupd-tMPI:
  extends:
    - .gromacs:base:regressiontest
  rules:
    - !reference [.rules:skip-if-dual-nvidia-gpus-unavailable, rules]
    - !reference [.rules:merge-requests-allow-failure, rules]
  image: ${CI_REGISTRY}/gromacs/gromacs/ci-ubuntu-22.04-gcc-12-cuda-12.1.0
  variables:
    CMAKE: /usr/local/cmake-3.29.8/bin/cmake
    REGRESSIONTEST_PME_RANK_NUMBER: 0
    REGRESSIONTEST_TOTAL_RANK_NUMBER: 4
    REGRESSIONTEST_OMP_RANK_NUMBER: 1
    GPU_VENDOR: "NVIDIA"
    GPU_COUNT: 2
  tags:
    - $GITLAB_RUNNER_TAG_2X_NVIDIA_GPU
  needs:
    - job: gromacs:gcc-12-cuda-12.1.0:build
    - job: gromacs:prepare
  artifacts:
    paths:
      - regressiontests
    when: always
    expire_in: 1 week
