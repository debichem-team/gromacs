GROMACS for Debian
------------------
This package ships with four variants of the core "gmx" program.

The standard version, simply "gmx", is compiled for single-precision floating
point operations.  For some applications, this may result in unacceptably large
numeric instabilities.  Double-precision calculations can often solve such
problems, with a tradeoff in speed (typically a ~30% slowdown on i386).  This
version is "gmx_d", and in all other ways operates identically to its single-
precision counterpart.

To parallelize calculations across multiple hosts, or in some cases across
multiple threads on a single host, you will need the MPI-enabled version of "gmx
mdrun". These are installed as "gmx_mpi" and "gmx_mpi_d", again in single and
double precision.  They use the Debian default MPI implementation for your
machine, currently OpenMPI where it exists, and MPICH otherwise - please check
the 'mpi-default-bin' package for information.


Additional Documentation
------------------------
In addition to the documentation included in this package, the GROMACS
website contains several additional manuals, tutorials, and checklists of
use to all skill levels, at <http://www.gromacs.org/Documentation>.


demux.pl and xplor2gmx.pl
-------------------------
You may see references to these scripts on the GROMACS mailing lists.
In this package, they have been renamed without their suffixes: demux
and xplor2gmx.


CPU Optimizations
-----------------
For the amd64 packages, the compiler is restricted to no more than SSE4.1 SIMD
extensions.  Most current systems will have additional SIMD extensions available
that can speed execution by large amounts, but which are not available on all
CPUs of that architecture (e.g. AVX extensions on amd64 that are only present on
Intel's Sandy Bridge or AMD Bulldozer and subsequent chips).  In recent years,
the optimum choices for Intel and AMD chips of similar age are often entirely
different, so there is no other consistently good default option for all users.
To compile a package with autodetection of available extensions, use the
environment variable DEB_BUILD_OPTIONS=cpuopt - but keep in mind that those
builds may not run on systems with different CPUs.

The SSE4.1 default works on nearly all amd64 CPUs, excluding only a few from
very early in the architecture lifetime, up until approximately 2009 for Intel
CPUs and 2011 for AMD.  These older systems are not recommended for GROMACS.

arm64 builds use NEON extensions that should be available on all systems.

For more information about GROMACS and CPU acceleration, please see
<http://www.gromacs.org/Documentation/Acceleration_and_parallelization>.


GPU Optimizations
-----------------
Additionally, the package can optionally be compiled with GPU acceleration
support.  This requires the additional packages "nvidia-cuda-toolkit" and
"nvidia-cuda-dev" from the non-free section of the archive; these are not part
of the normal build dependencies in order to keep GROMACS in the main section.

Set DEB_BUILD_OPTIONS=gpu to enable this feature.  If you're compiling and
running simulations on the same machine, you'll usually want to enable both this
option and the CPU optimizations above, by setting both options: 
DEB_BUILD_OPTIONS="cpuopt gpu"

While the upstream code is robust, this Debian build system option is not well
tested.  Bug reports, suggestions, and patches are always welcome.

For more information, please see
<https://manual.gromacs.org/documentation/current/user-guide/mdrun-performance.html#running-mdrun-with-gpus>.


 -- Nicholas Breen <nbreen@debian.org>, Fri, 26 Nov 2021 09:52:28 -0800
